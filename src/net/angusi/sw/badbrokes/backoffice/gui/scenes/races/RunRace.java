package net.angusi.sw.badbrokes.backoffice.gui.scenes.races;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.ComboBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.util.StringConverter;
import net.angusi.sw.badbrokes.BadbrokesBootstrap;
import net.angusi.sw.badbrokes.backoffice.gui.ControlWindow;
import net.angusi.sw.badbrokes.backoffice.gui.scenes.AbstractBackOfficePane;
import net.angusi.sw.badbrokes.common.RaceObjects.Bet;
import net.angusi.sw.badbrokes.common.RaceObjects.Horse;
import net.angusi.sw.badbrokes.common.RaceObjects.Person;
import net.angusi.sw.badbrokes.common.RaceObjects.Race;
import org.controlsfx.dialog.Dialogs;

import java.util.List;

public class RunRace extends AbstractBackOfficePane {
    public RunRace(Pane parentPane, Race raceToRun) {
        pane = new BorderPane();

        if(raceToRun.getIsRun()) {
            Dialogs.create()
                    .owner(pane)
                    .title("Read Only Race")
                    .masthead("This race has already been run!")
                    .message("The information here is shown for review only, and cannot be edited.\n")
                    .showInformation();
        }

        TableView<Bet> betsTable = new TableView<>();
        betsTable.setEditable(true);
        ObservableList<Bet> betsList = FXCollections.observableArrayList(raceToRun.getBets());
        betsTable.setItems(betsList);
        TableColumn<Bet, Person> betterID = new TableColumn<>("Better");
        betterID.setCellValueFactory(new PropertyValueFactory<>("better"));
        betterID.setCellFactory(ComboBoxTableCell.forTableColumn(new StringConverter<Person>() {
            @Override
            public String toString(Person object) {
                if(object != null) {
                    return object.nameProperty().getValue();
                } else {
                    return "";
                }
            }

            @Override
            public Person fromString(String string) {
                return null;
            }
        }, BadbrokesBootstrap.getHandle().getDataStore().getPeopleList()));
        betterID.setOnEditCommit(t -> t.getTableView().getItems().get(
                        t.getTablePosition().getRow()).setBetter(t.getNewValue())
        );
        TableColumn<Bet, Horse> horseID = new TableColumn<>("Horse");
        horseID.setCellValueFactory(new PropertyValueFactory<>("horse"));
        horseID.setCellFactory(ComboBoxTableCell.forTableColumn(new StringConverter<Horse>() {
            @Override
            public String toString(Horse object) {
                if (object != null) {
                    return object.nameProperty().getValue();
                } else {
                    return "";
                }
            }

            @Override
            public Horse fromString(String string) {
                return null;
            }
        }, raceToRun.getHorses()));
        horseID.setOnEditCommit(t -> t.getTableView().getItems().get(
                        t.getTablePosition().getRow()).setHorse(t.getNewValue())
        );
        TableColumn<Bet, Integer> stake = new TableColumn<>("Stake");
        stake.setCellValueFactory(new PropertyValueFactory<>("stake"));
        stake.setOnEditCommit(t -> t.getTableView().getItems().get(
                        t.getTablePosition().getRow()).setStake(t.getNewValue())
        );
        betsTable.getColumns().addAll(betterID, horseID, stake);


        ((BorderPane)pane).setCenter(betsTable);

        HBox buttonBar = new HBox();
        buttonBar.setAlignment(Pos.CENTER);
        Button addButton = new Button("Add Bet");
        addButton.setOnAction(e -> {
            Bet betToAdd = new Bet(raceToRun);
            List<Bet> bets = betsTable.itemsProperty().get();
            bets.add(betToAdd);
        });
        Button removeButton = new Button("Remove Bet");
        removeButton.setOnAction( e -> {
            if(betsTable.getSelectionModel().selectedIndexProperty().get() != -1) { betsTable.itemsProperty().get().remove(betsTable.getSelectionModel().selectedIndexProperty().get()); }
        });
        Button runButton = new Button("Complete & Run");
        Button backButton = new Button("Back");
        backButton.setOnAction(e -> ControlWindow.getHandle().setMainPane(parentPane));
        buttonBar.getChildren().addAll(addButton, removeButton, backButton);
        ((BorderPane)pane).setBottom(buttonBar);

    }
}
