package net.angusi.sw.badbrokes.backoffice.gui.scenes.races;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.*;
import net.angusi.sw.badbrokes.BadbrokesBootstrap;
import net.angusi.sw.badbrokes.backoffice.gui.ControlWindow;
import net.angusi.sw.badbrokes.backoffice.gui.scenes.AbstractBackOfficePane;
import net.angusi.sw.badbrokes.common.NumericEditableTableCell;
import net.angusi.sw.badbrokes.common.RaceObjects.Horse;
import net.angusi.sw.badbrokes.common.RaceObjects.Race;
import org.controlsfx.control.action.Action;
import org.controlsfx.dialog.Dialog;
import org.controlsfx.dialog.Dialogs;


public class EditRace extends AbstractBackOfficePane {
    public EditRace(Pane parentPane) {
        Race raceToEdit = new Race(FXCollections.observableArrayList());
        pane = new EditRace(parentPane, raceToEdit).getPane();
    }
    public EditRace(Pane parentPane, Race raceToEdit) {
        pane = new BorderPane();

        TableView<Horse> horsesTable = new TableView<>();
        horsesTable.setEditable(true);
        ObservableList<Horse> horsesList = FXCollections.observableArrayList(raceToEdit.getHorses());
        horsesTable.setItems(horsesList);

        TableColumn<Horse, String> horseName = new TableColumn<>("Horse Name");
        horseName.setCellValueFactory(new PropertyValueFactory<>("name"));
        horseName.setCellFactory(TextFieldTableCell.forTableColumn());
        horseName.setOnEditCommit(
                t -> t.getTableView().getItems().get(
                        t.getTablePosition().getRow()).nameProperty().set(t.getNewValue())
        );

        TableColumn oddsColls = new TableColumn("Odds");

        TableColumn<Horse, Long> odds1 = new TableColumn<>("");
        odds1.setCellValueFactory(new PropertyValueFactory<>("odds1"));
        odds1.setCellFactory(p -> new NumericEditableTableCell());
        odds1.setOnEditCommit(t -> t.getTableView().getItems().get(t.getTablePosition().getRow()).odds1Property().set(t.getNewValue().intValue()));


        TableColumn<Horse, String> oddsSeperator = new TableColumn<>("");
        oddsSeperator.setCellFactory(param -> new TableCell() {
            @Override
            protected void updateItem(Object item, boolean empty) {
                setText("/");
            }
        });
        oddsSeperator.setEditable(false);

        TableColumn<Horse, Long> odds2 = new TableColumn<>("");
        odds2.setCellValueFactory(new PropertyValueFactory<>("odds2"));
        odds2.setCellFactory(p -> new NumericEditableTableCell());
        odds2.setOnEditCommit(t -> t.getTableView().getItems().get(t.getTablePosition().getRow()).odds2Property().set(t.getNewValue().intValue()));

        oddsColls.getColumns().addAll(odds1, oddsSeperator, odds2);
        horsesTable.getColumns().addAll(horseName, oddsColls);

        ((BorderPane)pane).setCenter(horsesTable);

        HBox buttonBar = new HBox();
        buttonBar.setAlignment(Pos.CENTER);
        Button addHorseButton = new Button("Add Horse");
        addHorseButton.setOnAction(e -> horsesTable.itemsProperty().get().add(new Horse(0, 0, "")));
        Button removeHorseButton = new Button("Remove Horse");
        removeHorseButton.setOnAction(e -> {if(horsesTable.getSelectionModel().selectedIndexProperty().get() != -1) { horsesTable.itemsProperty().get().remove(horsesTable.getSelectionModel().selectedIndexProperty().get()); } });
        Separator buttonSeperator = new Separator(Orientation.VERTICAL);
        Button cancelButton = new Button("Cancel");
        cancelButton.setOnAction(e -> {
            if(horsesList.isEmpty()) {
                Action response = Dialogs.create()
                        .owner(pane)
                        .title("Empty Race")
                        .message("This race does not have any horses in it.\n Would you like to delete the entire race?")
                        .actions(Dialog.Actions.YES, Dialog.Actions.NO)
                        .showConfirm();
                if (response == Dialog.Actions.YES) {
                    BadbrokesBootstrap.getHandle().getDataStore().removeRace(raceToEdit.getRaceID());
                }
            }
            ControlWindow.getHandle().setMainPane(parentPane);
        });
        Button okButton = new Button("OK");
        okButton.setOnAction(e -> {
            if(horsesList.isEmpty()) {
                Action response = Dialogs.create()
                        .owner(pane)
                        .title("Empty Race")
                        .message("This race does not have any horses in it.\n Would you prefer to delete the entire race?")
                        .actions(Dialog.Actions.YES, Dialog.Actions.NO)
                        .showConfirm();
                if (response == Dialog.Actions.YES) {
                    BadbrokesBootstrap.getHandle().getDataStore().removeRace(raceToEdit.getRaceID());
                } else {
                    raceToEdit.getHorses().clear();
                    raceToEdit.getHorses().setAll(horsesList);
                }
            } else {
                raceToEdit.getHorses().clear();
                raceToEdit.getHorses().setAll(horsesList);
            }
            ControlWindow.getHandle().setMainPane(parentPane);
        });
        buttonBar.getChildren().addAll(addHorseButton, removeHorseButton, buttonSeperator, cancelButton, okButton);
        ((BorderPane)pane).setBottom(buttonBar);
    }
}
