package net.angusi.sw.badbrokes.backoffice.gui.scenes;

import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import net.angusi.sw.badbrokes.BadbrokesBootstrap;
import net.angusi.sw.badbrokes.backoffice.gui.ControlWindow;
import net.angusi.sw.badbrokes.common.NumericEditableTableCell;
import net.angusi.sw.badbrokes.common.RaceObjects.Person;
import org.controlsfx.control.action.Action;
import org.controlsfx.dialog.Dialog;
import org.controlsfx.dialog.Dialogs;

public class People extends AbstractBackOfficePane {
    public People(Pane parentPane) {
        pane = new BorderPane();

        TableView<Person> peopleTable = new TableView<>();
        peopleTable.setEditable(true);
        peopleTable.setItems(BadbrokesBootstrap.getHandle().getDataStore().getPeopleList());
        TableColumn<Person, Integer> personID = new TableColumn<>("Person ID");
        personID.setCellValueFactory(new PropertyValueFactory<>("personID"));
        personID.setEditable(false);
        TableColumn<Person, String> personName = new TableColumn<>("Name");
        personName.setCellValueFactory(new PropertyValueFactory<>("name"));
        personName.setCellFactory(TextFieldTableCell.forTableColumn());
        personName.setOnEditCommit(
                t -> t.getTableView().getItems().get(
                        t.getTablePosition().getRow()).setName(t.getNewValue(), "People table modified")
        );
        TableColumn<Person, Double> personMoney = new TableColumn<>("Balance");
        personMoney.setCellValueFactory(new PropertyValueFactory<>("money"));
        personMoney.setCellFactory(p -> new NumericEditableTableCell(false));

        peopleTable.getColumns().addAll(personID, personName, personMoney);
        ((BorderPane)pane).setCenter(peopleTable);

        HBox buttonBar = new HBox();
        buttonBar.setAlignment(Pos.CENTER);
        Button addPersonButton = new Button("New Person");
        addPersonButton.setOnAction(e -> new Person("", 0));
        Button deletePersonButton = new Button("Delete Person");
        deletePersonButton.setOnAction(e -> {
            if (peopleTable.getSelectionModel().selectedIndexProperty().getValue() != -1) {
                Action response = Dialogs.create()
                        .owner(pane)
                        .title("Are you sure?")
                        .message("Are you sure you wish to delete this person?")
                        .actions(Dialog.Actions.YES, Dialog.Actions.NO)
                        .showConfirm();
                if (response == Dialog.Actions.YES) {
                    BadbrokesBootstrap.getHandle().getDataStore().getPeople().remove(peopleTable.getSelectionModel().getSelectedItem().personIDProperty().get());
                }
            }
        });
        Button auditPersonButton = new Button("Audit Person");
        //Todo: AuditLog
        Button okButton = new Button("Back");
        okButton.setOnAction(e -> ControlWindow.getHandle().setMainPane(parentPane));
        buttonBar.getChildren().addAll(addPersonButton, deletePersonButton, auditPersonButton, okButton);
        ((BorderPane)pane).setBottom(buttonBar);
    }
}
