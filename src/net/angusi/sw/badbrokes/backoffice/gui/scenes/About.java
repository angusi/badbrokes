package net.angusi.sw.badbrokes.backoffice.gui.scenes;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import net.angusi.sw.badbrokes.BadbrokesBootstrap;
import net.angusi.sw.badbrokes.backoffice.gui.ControlWindow;

public class About extends AbstractBackOfficePane {
    public About(Pane parentPane) {
        pane = new VBox();
        ((VBox)pane).setAlignment(Pos.CENTER);

        Text headerText = new Text("This is "+ BadbrokesBootstrap.getAppTitle()+" version "+BadbrokesBootstrap.getAppVersion()+".\n\n");
        headerText.styleProperty().set("-fx-font-weight:bold");
        Text aboutText = new Text("Badbrokes attempts to implement a simple racenight bet manager.\n" +
                "Badbrokes was written by Angus Ireland - http://angusi.net\n\n"+
                "For more information, see the project's home at\n"+
                "  http://sw.angusi.net/badbrokes\n\n"+
                "Please see LICENSES.TXT (or the project Wiki) for applicable licenses.\n\n");
        Button okButton = new Button("Back");
        okButton.setOnAction(e -> ControlWindow.getHandle().setMainPane(parentPane));

        pane.getChildren().addAll(headerText, aboutText, okButton);


    }

}
