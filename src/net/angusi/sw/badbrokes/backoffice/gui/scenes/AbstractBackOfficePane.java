package net.angusi.sw.badbrokes.backoffice.gui.scenes;

import javafx.scene.Scene;
import javafx.scene.layout.Pane;

public abstract class AbstractBackOfficePane {
    protected Pane pane;

    public Pane getPane() {
        return pane;
    }
}
