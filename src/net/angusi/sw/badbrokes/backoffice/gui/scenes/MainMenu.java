package net.angusi.sw.badbrokes.backoffice.gui.scenes;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import net.angusi.sw.badbrokes.backoffice.gui.ControlWindow;
import net.angusi.sw.badbrokes.backoffice.gui.scenes.races.Races;

public class MainMenu extends AbstractBackOfficePane {
    public MainMenu(Pane parentPane) {
        pane = new VBox();
        ((VBox)pane).setAlignment(Pos.CENTER);

        HBox innerPane = new HBox();
        ((HBox)innerPane).setAlignment(Pos.CENTER);
        pane.getChildren().add(innerPane);

        GridPane buttonGridPane = new GridPane();
        buttonGridPane.setHgap(10);
        buttonGridPane.setVgap(10);
        buttonGridPane.setPadding(new Insets(0, 10, 0, 10));
        innerPane.getChildren().add(buttonGridPane);

        Button raceMenuButton = new Button("Races", new ImageView(new Image(getClass().getResourceAsStream("/net/angusi/sw/badbrokes/common/res/Races Icon small.png"))));
        raceMenuButton.setContentDisplay(ContentDisplay.TOP);
        raceMenuButton.setOnAction(e -> ControlWindow.getHandle().setMainPane(new Races(this.getPane()).getPane()));
        buttonGridPane.add(raceMenuButton, 1, 1);
        Button peopleMenuButton = new Button("People", new ImageView(new Image(getClass().getResourceAsStream("/net/angusi/sw/badbrokes/common/res/People Icon small.png"))));
        peopleMenuButton.setContentDisplay(ContentDisplay.TOP);
        peopleMenuButton.setOnAction(e -> ControlWindow.getHandle().setMainPane(new People(this.getPane()).getPane()));
        buttonGridPane.add(peopleMenuButton, 2, 1);
        //Button auditMenuButton = new Button("Audit", new ImageView(new Image(getClass().getResourceAsStream("/net/angusi/sw/badbrokes/common/res/Audit Icon small.png"))));
        //auditMenuButton.setContentDisplay(ContentDisplay.TOP);
        //buttonGridPane.add(auditMenuButton, 3, 1);
        Button aboutButton = new Button("About", new ImageView(new Image(getClass().getResourceAsStream("/net/angusi/sw/badbrokes/common/res/About Icon small.png"))));
        aboutButton.setContentDisplay(ContentDisplay.TOP);
        aboutButton.setOnAction(e -> ControlWindow.getHandle().setMainPane(new About(this.getPane()).getPane()));
        buttonGridPane.add(aboutButton, 1, 2);
        Button settingsButton = new Button("Settings", new ImageView(new Image(getClass().getResourceAsStream("/net/angusi/sw/badbrokes/common/res/Settings Icon small.png"))));
        settingsButton.setContentDisplay(ContentDisplay.TOP);
        buttonGridPane.add(settingsButton, 2, 2);
        //Button goRaceButton = new Button("Go Race!", new ImageView(new Image(getClass().getResourceAsStream("/net/angusi/sw/badbrokes/common/res/Go Race Icon small.png"))));
        //goRaceButton.setContentDisplay(ContentDisplay.TOP);
        //buttonGridPane.add(goRaceButton, 3, 2);
    }
}
