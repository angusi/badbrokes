package net.angusi.sw.badbrokes.backoffice.gui.scenes.races;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import net.angusi.sw.badbrokes.BadbrokesBootstrap;
import net.angusi.sw.badbrokes.backoffice.gui.scenes.AbstractBackOfficePane;
import net.angusi.sw.badbrokes.common.RaceObjects.Horse;
import net.angusi.sw.badbrokes.common.RaceObjects.Race;
import net.angusi.sw.badbrokes.backoffice.gui.ControlWindow;
import org.controlsfx.control.action.Action;
import org.controlsfx.dialog.Dialog;
import org.controlsfx.dialog.Dialogs;

import java.util.List;

public class Races extends AbstractBackOfficePane {
    public Races(Pane parentPane) {
        pane = new BorderPane();

        TableView<Race> racesTable = new TableView<>();
        racesTable.setItems(BadbrokesBootstrap.getHandle().getDataStore().getRacesList());
        TableColumn<Race, Integer> raceID = new TableColumn<>("Race ID");
        raceID.setCellValueFactory(new PropertyValueFactory<Race, Integer>("raceID"));
        TableColumn<Race, List> horsesList = new TableColumn<>("Runners");
        horsesList.setCellValueFactory(new PropertyValueFactory<Race, List>("horses"));
        horsesList.setCellFactory(param -> new TableCell<Race, List>() {
            @Override
            protected void updateItem(List horses, boolean empty) {
                if(horses != null && horses.size() > 0) {
                    StringBuilder horseNames = new StringBuilder();
                    for (Object thisHorse : horses) {
                        horseNames.append(((Horse) thisHorse).getName());
                        horseNames.append(", ");
                    }
                    setText(horseNames.substring(0, horseNames.length() - 2));
                } else {
                    setText("(No horses)");
                }
            }
        });
        racesTable.getColumns().addAll(raceID, horsesList);
        ((BorderPane)pane).setCenter(racesTable);

        HBox buttonBar = new HBox();
        buttonBar.setAlignment(Pos.CENTER);
        Button addRaceButton = new Button("New Race");
        addRaceButton.setOnAction(e -> ControlWindow.getHandle().setMainPane(new EditRace(this.getPane()).getPane()));
        Button editRaceButton = new Button("Edit Race");
        editRaceButton.setOnAction(e -> { if(racesTable.getSelectionModel().selectedIndexProperty().getValue() != -1) {
            if(BadbrokesBootstrap.getHandle().getDataStore().getRaces().get(racesTable.getSelectionModel().getSelectedItem().getRaceID()).getIsRun()) {
                Dialogs.create()
                        .owner(pane)
                        .title("Cannot edit race")
                        .message("This race has already run, and so it cannot be edited.")
                        .showError();
            } else {
                ControlWindow.getHandle().setMainPane(new EditRace(this.getPane(), BadbrokesBootstrap.getHandle().getDataStore().getRaces().get(racesTable.getSelectionModel().getSelectedItem().getRaceID())).getPane());
            }
        }});
        Button deleteRaceButton = new Button("Delete Race");
        deleteRaceButton.setOnAction(e -> { if(racesTable.getSelectionModel().selectedIndexProperty().getValue() != -1) {
            Action response = Dialogs.create()
                    .owner(pane)
                    .title("Are you sure?")
                    .message("Are you sure you wish to delete this race?")
                    .actions(Dialog.Actions.YES, Dialog.Actions.NO)
                    .showConfirm();
            if(response == Dialog.Actions.YES) {
                BadbrokesBootstrap.getHandle().getDataStore().getRaces().remove(racesTable.getSelectionModel().getSelectedItem().getRaceID());
            }
        }});
        //Todo:
        Button auditRaceButton = new Button("Audit Race");

        Button runRaceButton = new Button("Run Race");
        runRaceButton.setOnAction(e -> {
            if (racesTable.getSelectionModel().selectedIndexProperty().getValue() != -1) {
                ControlWindow.getHandle().setMainPane(new RunRace(this.getPane(), BadbrokesBootstrap.getHandle().getDataStore().getRaces().get(racesTable.getSelectionModel().getSelectedItem().getRaceID())).getPane());
            }
        });

        Button okButton = new Button("Back");
        okButton.setOnAction(e -> ControlWindow.getHandle().setMainPane(parentPane));
        buttonBar.getChildren().addAll(addRaceButton, editRaceButton, deleteRaceButton, runRaceButton, auditRaceButton, okButton);
        ((BorderPane)pane).setBottom(buttonBar);
    }
}
