package net.angusi.sw.badbrokes.backoffice.gui;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import net.angusi.sw.badbrokes.BadbrokesBootstrap;
import net.angusi.sw.badbrokes.backoffice.gui.scenes.MainMenu;
import net.angusi.sw.badbrokes.backoffice.gui.scenes.People;
import net.angusi.sw.badbrokes.backoffice.gui.scenes.races.Races;
import net.angusi.sw.badbrokes.display.gui.DisplayWindow;
import org.controlsfx.control.action.Action;
import org.controlsfx.dialog.Dialog;
import org.controlsfx.dialog.Dialogs;

public class ControlWindow {
    private static ControlWindow classHandle;
    private final Stage stage;
    private final BorderPane mainPanel;

    private ControlWindow() {
        System.out.println("Setting up Control (Back Office) Window");

        stage = new Stage();
        stage.setTitle("Badbrokes Betting"); //TODO: Make preference?
        stage.setMinHeight(480);
        stage.setMinWidth(640);
        stage.initStyle(StageStyle.DECORATED);

        mainPanel = new BorderPane();
        Scene scene = new Scene(mainPanel);
        stage.setScene(scene);
        stage.setOnCloseRequest(e -> {
            Action response = Dialogs.create()
                    .owner(stage)
                    .title("Quit Badbrokes?")
                    .masthead("You are about to close your Badbrokes session.")
                    .message("Note that your session will not be saved.\nAre you sure you want to quit?")
                    .actions(Dialog.Actions.OK, Dialog.Actions.CANCEL)
                    .showConfirm();
            if (response == Dialog.Actions.OK) {
                BadbrokesBootstrap.quitApplication();
            } else {
               e.consume();
            }
        });

        setUpToolbars();

        showMainMenu();
    }

    private void setUpToolbars() {
        HBox displayControls = new HBox();
        displayControls.setAlignment(Pos.CENTER);

        Button showButton = new Button("Show Display");
        showButton.setOnAction(e -> DisplayWindow.getHandle().getStage().show());

        Button hideButton = new Button("Hide Display");
        hideButton.setOnAction(e -> DisplayWindow.getHandle().getStage().hide());

        Button blankButton = new Button("Blank Display");
        blankButton.setOnAction(e -> DisplayWindow.getHandle().resetWindowContents());

        displayControls.getChildren().addAll(showButton, hideButton, blankButton);

        mainPanel.setTop(displayControls);


        HBox menuControls = new HBox();
        menuControls.setAlignment(Pos.CENTER);

        Button mainMenuButton = new Button("Main Menu");
        mainMenuButton.setOnAction(e -> showMainMenu());

        Button raceMenuButton = new Button("Manage Races");
        raceMenuButton.setOnAction(e -> setMainPane(new Races(getMainPane()).getPane()));

        Button peopleMenuButton = new Button("Manage People");
        peopleMenuButton.setOnAction(e -> setMainPane(new People(getMainPane()).getPane()));

        //Button auditMenuButton = new Button("Audit Logs");

        Button settingsButton = new Button("Settings");

        //Button goRaceButton = new Button("Go Race!");


        menuControls.getChildren().addAll(mainMenuButton, raceMenuButton, peopleMenuButton, settingsButton);

        mainPanel.setBottom(menuControls);
    }

    private void showMainMenu() {
        mainPanel.setCenter(new MainMenu(null).getPane());
    }

    public void setMainPane(Pane pane) {
        mainPanel.setCenter(pane);
    }

    public Pane getMainPane() {
        return (Pane)mainPanel.getCenter();
    }

    public static ControlWindow getHandle() {
        if(classHandle == null) {
            classHandle = new ControlWindow();
        }
        return classHandle;
    }

    public Stage getStage() {
        return stage;
    }
}
