package net.angusi.sw.badbrokes;

import javafx.application.Application;
import javafx.stage.Stage;
import net.angusi.sw.badbrokes.backoffice.gui.ControlWindow;
import net.angusi.sw.badbrokes.common.datastore.DataStore;

public class BadbrokesBootstrap extends Application {

    private static final String APPTITLE = "Badbrokes";
    private static final String APPVERSION = "2.0";
    private static BadbrokesBootstrap handle;

    private DataStore dataStore;

    public static void main(final String args[]) {

        System.out.println(APPTITLE+" version "+APPVERSION);

        System.out.println("Initialising JavaFX");
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        dataStore = new DataStore(); //Todo: reload last datastore on launch?
        ControlWindow.getHandle().getStage().show();
        handle=this;
    }

    public static void quitApplication() {
        System.out.println("Terminating.");
        System.exit(0);
    }

    public static String getAppTitle() {
        return APPTITLE;
    }

    public static String getAppVersion() {
        return APPVERSION;
    }

    public static BadbrokesBootstrap getHandle() {
        return handle;
    }

    public DataStore getDataStore() {
        return dataStore;
    }
}
