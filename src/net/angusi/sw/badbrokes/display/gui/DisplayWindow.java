package net.angusi.sw.badbrokes.display.gui;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import net.angusi.sw.badbrokes.BadbrokesBootstrap;
import net.angusi.sw.badbrokes.common.RaceObjects.Horse;

import java.util.List;

public class DisplayWindow {

    private static DisplayWindow classHandle;
    private final Stage stage;

    private final VBox mainPanel;

    private DisplayWindow() {
        System.out.println("Setting up Display (Projector) Window");

        stage = new Stage();
        stage.setTitle(BadbrokesBootstrap.getHandle().getDataStore().getBookmakerName());
        stage.setMinHeight(480);
        stage.setMinWidth(640);
        stage.setMaximized(true);
        //stage.hide();
        stage.initStyle(StageStyle.UTILITY);



        mainPanel = new VBox();
        mainPanel.setBackground(new Background(new BackgroundFill(BadbrokesBootstrap.getHandle().getDataStore().getBackgroundColor(), CornerRadii.EMPTY, Insets.EMPTY)));
        mainPanel.setAlignment(Pos.CENTER);
        Scene scene = new Scene(mainPanel, BadbrokesBootstrap.getHandle().getDataStore().getBackgroundColor());
        stage.setScene(scene);

        showGetReady();
    }

    //Removes everything except the Badbrokes logo.
    public void resetWindowContents() {
        ImageView badbrokesLogo;
        if(BadbrokesBootstrap.getHandle().getDataStore().getImagePath().equals("")) {
            badbrokesLogo = new ImageView("/net/angusi/sw/badbrokes/common/res/Badbrokes Logo.png");
        } else {
            badbrokesLogo = new ImageView(BadbrokesBootstrap.getHandle().getDataStore().getImagePath()); //TODO: Error handling for missing image?
        }

        //Auto-scaling:
        badbrokesLogo.setPreserveRatio(true);
        badbrokesLogo.fitWidthProperty().bind(mainPanel.widthProperty());

        mainPanel.getChildren().clear();
        mainPanel.getChildren().add(badbrokesLogo);
    }

    //Resets the window and adds the "Get Ready!" text
    void showGetReady() {
        resetWindowContents();

        Text badbrokesSlogan = new Text(BadbrokesBootstrap.getHandle().getDataStore().getGetReadyText());
        badbrokesSlogan.setFill(BadbrokesBootstrap.getHandle().getDataStore().getForegroundColor());

        //Auto-scaling:
        mainPanel.widthProperty().addListener((observable, oldValue, newValue) -> resizeText(badbrokesSlogan, mainPanel.widthProperty().doubleValue(), mainPanel.heightProperty().doubleValue()/2));
        mainPanel.heightProperty().addListener((observable, oldValue, newValue) -> resizeText(badbrokesSlogan, mainPanel.widthProperty().doubleValue(), mainPanel.heightProperty().doubleValue()/2));
        mainPanel.getChildren().addAll(badbrokesSlogan);

    }

    public void showOdds(List<Horse> horsesList) {
        //TODO: If odds are moved out of horses, then fix this.
        resetWindowContents();

        ObservableList<Horse> horses = FXCollections.observableArrayList(horsesList);

        TableView<Horse> oddsTable = new TableView<>();
        oddsTable.setItems(horses);

        TableColumn<Horse, String> nameTableColumn = new TableColumn<>("Horse");
        nameTableColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        TableColumn<Horse, Integer> odds1TableColumn = new TableColumn<>("For");
        odds1TableColumn.setCellValueFactory(new PropertyValueFactory<>("odds1"));
        TableColumn<Horse, String> separatorColumn = new TableColumn<>("");
        separatorColumn.setCellFactory(param -> new TableCell() {
            @Override
            protected void updateItem(Object item, boolean empty) {
                setText("/");
            }
        });
        TableColumn<Horse, Integer> odds2TableColumn = new TableColumn<>("Against");
        odds2TableColumn.setCellValueFactory(new PropertyValueFactory<>("odds2"));

        oddsTable.getColumns().setAll(nameTableColumn, odds1TableColumn, separatorColumn, odds2TableColumn);
        oddsTable.setMouseTransparent(true);

        mainPanel.getChildren().add(oddsTable);

    }

    //Resizes text so that it fits inside the maxWidth/maxHeight specified
    private void resizeText(Text textToResize, double maxWidth, double maxHeight) {
        while(textToResize.getBoundsInLocal().getWidth() < maxWidth && textToResize.getBoundsInLocal().getHeight() < maxHeight) {
            textToResize.setFont(Font.font(textToResize.getFont().getSize()+1));
        }
        while(textToResize.getBoundsInLocal().getWidth() > maxWidth || textToResize.getBoundsInLocal().getHeight() > maxHeight) {
            textToResize.setFont(Font.font(textToResize.getFont().getSize()-1));
        }
    }

    public static DisplayWindow getHandle() {
        if(classHandle == null) {
            classHandle = new DisplayWindow();
        }
        return classHandle;
    }

    public Stage getStage() {
        return stage;
    }

    //TODO: Show results - depends on rewrite of RaceObjects.Race...
    /*
    public void showResults(ArrayList<Horse> horses) {
        mainWindow.remove(mainPanel);
        mainPanel = new JPanel();
        mainPanel.setLayout(new GridBagLayout());

        mainPanelGBC = new GridBagConstraints();
        mainPanel.setBackground(Color.RED);
        mainWindow.add(mainPanel, BorderLayout.CENTER);

        for(int i = 0; i<horses.size(); i++) {
            JPanel resultPanel = new JPanel(new BorderLayout());
            ResizableJLabel result = new ResizableJLabel(String.valueOf(i+1)+".", SwingConstants.CENTER);
            result.setForeground(Color.WHITE);
            resultPanel.setBackground(Color.RED);
            mainPanelGBC.gridy=i;
            mainPanelGBC.gridx=0;
            mainPanelGBC.gridwidth=1;
            mainPanelGBC.gridheight=1;
            mainPanelGBC.fill = GridBagConstraints.BOTH;
            mainPanelGBC.weightx = 0.25;
            mainPanelGBC.weighty = 1/horses.size();
            mainPanel.add(resultPanel, mainPanelGBC);
            resultPanel.add(result, BorderLayout.CENTER);

            JPanel thisHorsePanel = new JPanel(new BorderLayout());
            ResizableJLabel thisHorse = new ResizableJLabel(horses.get(i).getName(), SwingConstants.CENTER);
            thisHorse.setForeground(Color.WHITE);
            thisHorsePanel.setBackground(Color.RED);
            mainPanelGBC.gridy=i;
            mainPanelGBC.gridx=1;
            mainPanelGBC.gridwidth=2;
            mainPanelGBC.gridheight=1;
            mainPanelGBC.fill = GridBagConstraints.BOTH;
            mainPanelGBC.weightx = 0.75;
            mainPanelGBC.weighty = 1/horses.size();
            mainPanel.add(thisHorsePanel, mainPanelGBC);
            thisHorsePanel.add(thisHorse, BorderLayout.CENTER);
        }
        mainWindow.pack();
        mainWindow.setExtendedState(mainWindow.getExtendedState() | JFrame.MAXIMIZED_BOTH);
    }*/
}
