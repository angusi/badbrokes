package net.angusi.sw.badbrokes.common.datastore;

import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.MapChangeListener;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import javafx.scene.paint.Color;
import net.angusi.sw.badbrokes.common.RaceObjects.Person;
import net.angusi.sw.badbrokes.common.RaceObjects.Race;

import java.io.*;
import java.util.ArrayList;
import java.util.Map;

public class DataStore implements Serializable {

    //Races
    private final ObservableMap<Integer, Race> races;
    private int raceID = 0;

    //People
    private final ObservableMap<Integer, Person> people;
    private int personID = 0;

    //Customisation
    private StringProperty bookmakerName;
    private ObjectProperty<Color> backgroundColor;
    private ObjectProperty<Color> foregroundColor;
    private StringProperty imagePath;
    private StringProperty getReadyText;

    public DataStore() {
        races = FXCollections.observableHashMap();
        people = FXCollections.observableHashMap();
        bookmakerName = new SimpleStringProperty("Badbrokes");
        backgroundColor = new SimpleObjectProperty<>(Color.RED);
        foregroundColor = new SimpleObjectProperty<>(Color.WHITE);
        imagePath = new SimpleStringProperty("");
        getReadyText = new SimpleStringProperty("Get Ready!");
    }

    public int getNextPersonID() {
        return personID++;
    }

    public int getNextRaceID() {
        return raceID++;
    }

    public ObservableMap<Integer, Race> getRaces() {
      return races;
    }

    public ObservableList<Race> getRacesList() {
        ObservableList<Race> racesList = FXCollections.observableArrayList(races.values());
        races.addListener((Observable observable) -> {
            racesList.clear();
            racesList.addAll(races.values());
        });
        return racesList;
    }

    public ObservableMap<Integer, Person> getPeople() {
        return people;
    }

    public ObservableList<Person> getPeopleList() {
        ObservableList<Person> peopleList = FXCollections.observableArrayList(people.values());
        people.addListener((Observable observable) -> {
            peopleList.clear();
            peopleList.addAll(people.values());
        });
        return peopleList;
    }

    public String getBookmakerName() {
        return bookmakerName.get();
    }

    public StringProperty bookmakerNameProperty() {
        return bookmakerName;
    }

    public Color getBackgroundColor() {
        return backgroundColor.get();
    }

    public ObjectProperty<Color> backgroundColorProperty() {
        return backgroundColor;
    }

    public Color getForegroundColor() {
        return foregroundColor.get();
    }

    public ObjectProperty<Color> foregroundColorProperty() {
        return foregroundColor;
    }

    public String getImagePath() {
        return imagePath.get();
    }

    public StringProperty imagePathProperty() {
        return imagePath;
    }

    public String getGetReadyText() {
        return getReadyText.get();
    }

    public StringProperty getReadyTextProperty() {
        return getReadyText;
    }

    public void setBookmakerName(String bookmakerName) {
        this.bookmakerName.set(bookmakerName);
    }

    public void setBackgroundColor(Color backgroundColor) {
        this.backgroundColor.set(backgroundColor);
    }

    public void setForegroundColor(Color foregroundColor) {
        this.foregroundColor.set(foregroundColor);
    }

    public void setImagePath(String imagePath) {
        this.imagePath.set(imagePath);
    }

    public void setGetReadyText(String getReadyText) {
        this.getReadyText.set(getReadyText);
    }

    public int addRace(Race raceToAdd) {
        int raceID = getNextRaceID();
        races.put(raceID, raceToAdd);
        return raceID;
    }

    public int addPerson(Person personToAdd) {
        int personID = getNextPersonID();
        people.put(personID, personToAdd);
        return personID;
    }

    public void removeRace(int raceIDToRemove) {
        races.remove(raceIDToRemove);
    }

    public void removePerson(int personIDToRemove) {
        people.remove(personIDToRemove);
    }

    public static boolean saveDataStore(DataStore storeToSave, String savePath) throws SecurityException, FileNotFoundException {
        FileOutputStream fileOut = new FileOutputStream(savePath);
        ObjectOutputStream out;
        try {
            out = new ObjectOutputStream(fileOut);
            out.writeObject(storeToSave);
            out.close();
            fileOut.close();
        }
        catch (IOException e) {
            return false;
        }
        System.out.println("Saved session data to " + savePath + ".");
        return true;
    }

    public static DataStore loadDataStore(String loadPath) throws FileNotFoundException, ClassNotFoundException {
        DataStore loadedStore;
        FileInputStream fileIn = new FileInputStream(loadPath);
        ObjectInputStream in;
        try
        {
            in = new ObjectInputStream(fileIn);
            loadedStore = (DataStore) in.readObject();
            in.close();
            fileIn.close();
        } catch (IOException e) {
            return null;
        }
        System.out.println("Loaded session data from "+loadPath + ".");
        return loadedStore;
    }
}
