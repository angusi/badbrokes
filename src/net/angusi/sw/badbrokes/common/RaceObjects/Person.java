package net.angusi.sw.badbrokes.common.RaceObjects;

import javafx.beans.property.*;
import net.angusi.sw.badbrokes.BadbrokesBootstrap;

import java.io.Serializable;

public class Person extends AuditableObject implements Serializable {
    private final ReadOnlyStringWrapper name = new ReadOnlyStringWrapper();
    private final ReadOnlyDoubleWrapper money = new ReadOnlyDoubleWrapper();
    private final ReadOnlyIntegerWrapper personID = new ReadOnlyIntegerWrapper();

    public Person(String name, int money) {
        this.name.set(name);
        this.money.set(money);
        this.personID.set( BadbrokesBootstrap.getHandle().getDataStore().addPerson(this));
        auditLog.add("Person #"+this.personID.get()+" created with name "+name+" and money "+money+".");
    }


    public ReadOnlyStringProperty nameProperty() {
        return name.getReadOnlyProperty();
    }

    public ReadOnlyDoubleProperty moneyProperty() {
        return money.getReadOnlyProperty();
    }

    public ReadOnlyIntegerProperty personIDProperty() {
        return personID.getReadOnlyProperty();
    }


    public void setName(String name, String reason) {
        auditLog.add("#"+this.personID.get()+"'s name changed from  "+this.name+" to "+name+" - "+reason);
        this.name.set(name);

    }

    public void setMoney(double money, String reason) {
        auditLog.add("#"+this.personID.get()+" ("+this.name.get()+")'s money changed from  "+this.money+" to "+money+" - "+reason);
        this.money.set(money);
    }

    public void setMoney(long money, String reason) {
        auditLog.add("#"+this.personID.get()+" ("+this.name.get()+")'s money changed from  "+this.money+" to "+money+" - "+reason);
        this.money.set(money);
    }

    public void addMoney(double money, String reason) {
        auditLog.add("#"+this.personID.get()+" ("+this.name.get()+")'s money changed from  "+this.money.getValue()+" to "+(this.money.getValue()+money)+" (+"+money+") - "+reason);
        this.money.set(this.money.getValue()+money);
    }

    public void removeMoney(double money, String reason) {
        auditLog.add("#"+this.personID.get()+" ("+this.name.get()+")'s money changed from  "+this.money.getValue()+" to "+(this.money.getValue()-money)+" (-"+money+") - "+reason);
        this.money.set(this.money.getValue()-money);
    }
}
