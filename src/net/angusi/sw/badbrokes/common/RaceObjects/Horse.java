package net.angusi.sw.badbrokes.common.RaceObjects;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.io.Serializable;

public class Horse implements Serializable {
    private final StringProperty name = new SimpleStringProperty();
    private final IntegerProperty odds1 = new SimpleIntegerProperty();
    private final IntegerProperty odds2 = new SimpleIntegerProperty();

    public Horse(int odds1, int odds2, String name) {
        this.name.set(name);
        this.odds1.set(odds1);
        this.odds2.set(odds2);
    }

    public String getName() {
        return name.getValue();
    }

    public StringProperty nameProperty() {
        return name;
    }

    public int getOdds1() {
        return odds1.getValue();
    }

    public IntegerProperty odds1Property() {
        return odds1;
    }

    public int getOdds2() {
        return odds2.getValue();
    }

    public IntegerProperty odds2Property() {
        return odds2;
    }
}
