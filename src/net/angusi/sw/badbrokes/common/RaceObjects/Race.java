package net.angusi.sw.badbrokes.common.RaceObjects;

import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import net.angusi.sw.badbrokes.BadbrokesBootstrap;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Race extends AuditableObject implements Serializable {
    private ListProperty<Horse> horses = new SimpleListProperty<>();
    private ListProperty<Bet> bets = new SimpleListProperty<>();
    private BooleanProperty isRun = new SimpleBooleanProperty(false);
    private final ReadOnlyIntegerWrapper raceID = new ReadOnlyIntegerWrapper();

    public Race(List<Horse> horses) {
        this.horses.set(FXCollections.observableList(horses));
        this.bets.set(FXCollections.observableArrayList());
        raceID.set(BadbrokesBootstrap.getHandle().getDataStore().addRace(this));
        auditLog.add("Race #"+this.raceID.get()+" created.");
        System.out.println("Race #"+this.raceID.get()+" created.");

    }

    public ObservableList<Horse> getHorses() {
        return horses.getValue();
    }

    public ListProperty<Horse> horsesProperty() {
        return horses;
    }

    public ObservableList<Bet> getBets() {
        return bets.getValue();
    }

    public ListProperty<Bet> betsProperty() {
        return bets;
    }

    public int getRaceID() {
        return raceID.getValue();
    }

    public ReadOnlyIntegerProperty raceIDProperty() {
        return raceID.getReadOnlyProperty();
    }

    public boolean getIsRun() {
        return isRun.getValue();
    }

    public BooleanProperty isRunProperty() {
        return isRun;
    }

    public ArrayList<Bet>[] runRace(ArrayList<Horse> horsePlacing) {
        ArrayList<Bet> winningBets = new ArrayList<>();
        ArrayList<Bet> losingBets = new ArrayList<>();


        for(int i = 0; i < horsePlacing.size(); i++) {
            //For every horse, in order of finishing, log it...
            this.auditLog.add(horsePlacing.get(i).getName()+" finishes in place "+i);
            //...then work out if anyone had a winning bet on this horse
            for(Bet thisBet : bets) {
                if(thisBet.getPlacing() == (i+1)) {
                    if(thisBet.getHorse() == horsePlacing.get(i)) {
                        thisBet.winningBet();
                        winningBets.add(thisBet);
                        this.auditLog.add(thisBet.getBetter().nameProperty().toString()+" wins "+(thisBet.getPrize()+thisBet.getStake())+" ("+thisBet.getPrize() +" and the "+thisBet.getStake()+" stake) for horse "+thisBet.getHorse().getName());
                    }
                    else {
                        losingBets.add(thisBet);
                        this.auditLog.add(thisBet.getBetter().nameProperty().toString()+" loses "+thisBet.getStake()+" for horse "+thisBet.getHorse().getName());
                    }
                }
            }
        }
        isRun.set(true);
        return new ArrayList[]{winningBets, losingBets};
    }
}
