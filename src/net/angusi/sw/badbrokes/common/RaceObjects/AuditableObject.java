package net.angusi.sw.badbrokes.common.RaceObjects;

import java.util.ArrayList;

abstract class AuditableObject {
    final ArrayList<String> auditLog = new ArrayList<>();

    public ArrayList<String> getAuditLog() {
        return auditLog;
    }
}
