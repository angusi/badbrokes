package net.angusi.sw.badbrokes.common.RaceObjects;

import java.io.Serializable;

public class Bet implements Serializable {
    private Person better;
    private Horse horse;
    private long stake;
    private int placing;
    private final Race race;

    public Bet(Race race) {
        this.race = race;
    }

    public void confirmBet(Person better, Horse horse, long stake, int placing) {
        this.better = better;
        this.horse = horse;
        this.stake = stake;
        this.placing = placing;
        better.removeMoney(stake, "Placed bet on horse "+horse.getName()+" in race #"+race.getRaceID());
    }

    public Person getBetter() {
        return better;
    }

    public float getPrize() {
        return (stake*horse.getOdds1())/horse.getOdds2();
    }

    public float getStake() {
        return stake;
    }

    public Horse getHorse() {
        return horse;
    }

    public int getPlacing() {
       return placing;
    }

    public void setBetter(Person better) {
        this.better = better;
    }

    public void setHorse(Horse horse) {
        this.horse = horse;
    }

    public void setStake(long stake) {
        this.stake = stake;
    }

    public void setPlacing(int placing) {
        this.placing = placing;
    }

    public void winningBet() {
        long winnings = (stake*horse.getOdds1())/horse.getOdds2();
        if(this.placing != 1) {
            winnings = winnings/4; //Place Bets get 1/4 odds
        }
        winnings += stake;
        better.addMoney(winnings, "Winning Bet on horse "+horse.getName()+" in race "+race.getRaceID());
    }
}
