# Badbrokes Betting Manager
*Simple Racenight Management software*

## What is Badbrokes?
Badbrokes attempts to implement a simple racenight bet manager.

It is intended to be used in a multi-screen environment, where the Bookmaker (or an assistant) creates the Races to be run in the application's management window on one display, whilst the information is presented in a "nice" manner to the betting customers on another display or projector.

##What features does it include?
Races can be created on the fly - during the event - or (in a future update) before hand, and loaded up ready to run.

The software also features "customer management". Customers - individuals or teams, depending on the basis of your race night - can be created within the application and their balances can be managed, allowing the bookmaker to see at a glance how much a customer has won (or indeed lost) during the course of the event. When this feature is used, the application will also calculate payouts for the bookmaker, including Each Way and Place bets, easing the burden of that pesky mathematics thing.

Indeed, if the race night is just for fun, the bookmaker can "float" each customers account with a virtual balance, and this can be used instead of (or as well as!) physical subsitute currency.

Full audit logging is also provided, so that each stage of the night can be recounted, and this can be filtered by customer, race or horse.

## Requirements
 * Java 8
 * Sound Ouput Device


## Disclaimer
*This application is not affiliated to or connected with any real bookmaking entity, and any resemblance to real bookmakers is entirely coincidental, obviously...*

Gamble safe, kids.